var menuButton = document.getElementById('menuButton');
var sideBar = document.getElementById('leftAside');
var sideBarLocalStorage = localStorage.getItem('style');
var searchButton = document.getElementById('searchButton');

menuButton.addEventListener('click', function(){ //hide/unhide the sidebar on click
    if(sideBar.style.display != 'none' && content.style.width != '100%'){
        sideBar.style.display = 'none';
        content.style.width = '100%';
    }else{
        sideBar.style.display = 'block';
        content.style.width = '82.44%';
    }
   
});

searchButton.addEventListener('click', function(){   //empty search on click
    searchInput.value = ''
});

function colorChanger(buttonId, fasId){ //changes buttons color from gray to red or opposite
    buttonId.addEventListener('click', function(){
        if(fasId.style.color == 'red'){
            fasId.style.color = '#7c7a7a'
        }else{
            fasId.style.color = 'red';
        }
    });
}

createVideoBtn.addEventListener('click', function(){
    YouTubeTv.style.display = 'none';
    YouTubeSignInChart.style.display = 'none';
    if(createVideo.style.display == 'block'){
        createVideo.style.display = 'none'
    }else{
        createVideo.style.display = 'block'
    }
});
YouTubeTvBtn.addEventListener('click', function(){
    createVideo.style.display = 'none';
    YouTubeSignInChart.style.display = 'none'
   if(YouTubeTv.style.display == 'block'){
        YouTubeTv.style.display = 'none';
   }else{
       YouTubeTv.style.display = 'block';
   }
});
YouTubeChartButton.addEventListener('click', function(){
    createVideo.style.display = 'none';
    YouTubeTv.style.display = 'none';
    if(YouTubeSignInChart.style.display == 'block'){
        YouTubeSignInChart.style.display = 'none'
    }else{
        YouTubeSignInChart.style.display = 'block';
    }
});

var buttonsArray = [navButton1, navButton2, navButton3, navButton4, 
                    navButton5, navButton6, navButton7, navButton8, 
                    navButton9, navButton10, navButton11, navButton12, 
                    navButton13, navButton14, navButton15, navButton16, 
                    navButton17]
var navFasArray = [navFa1, navFa2, navFa3, navFa4, navFa5, navFa6, navFa7, 
                    navFa8, navFa9, navFa10, navFa11, navFa12, navFa13, navFa14, 
                    navFa15, navFa16, navFa17]
for(var i = 0; i< buttonsArray.length; i++){
    colorChanger(buttonsArray[i], navFasArray[i])
};

function contentMouseOver(content, image, giphy){
    content.addEventListener('mouseenter', function(){
        image.style.display = 'none'
        giphy.style.display = 'block'
    })
    content.addEventListener('mouseleave', function(){
        image.style.display = 'block'
        giphy.style.display = 'none'
    })
}
var contentArray = [contentItem, contentItem2, contentItem3, contentItem4, contentItem5, contentItem6];
var imageArray = [image, image2, image3, image4, image5, image6]
var giphyArray = [giphy, giphy2, giphy3, giphy4, giphy5, giphy6]
for( var i = 0; i < contentArray.length; i++){
    contentMouseOver(contentArray[i], imageArray[i], giphyArray[i])
}


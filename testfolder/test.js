var vid = document.getElementById('myVideo');
var playBtn = document.getElementById('playBtn')
var fillbar = document.getElementById('fill')
var currentTime = document.getElementById('currentTime')
var volumeSlider = document.getElementById('volume')
function playOrPause(){
   if(vid.paused){
      vid.play();
      playBtn.className = "fas fa-stop-circle"
   }else{
      vid.pause();
      playBtn.className = "fas fa-play"
      
   }

}
vid.addEventListener('timeupdate', function(){
   var position = vid.currentTime / vid.duration;
   fillbar.style.width = position * 100 +"%";
})

function changeVolume(){
   vid.volume = volumeSlider.value
}